Programme's Notes
-----------------

General:
	- Total gameplay revamp. My god, this map blows in its current state.
		- Implement puzzles
		- Implement metrocop firefights
		- Revamp Manhacks
		- Remove or reutilize the func_ladder fence climbing mechanic. It's very awkward in its current state
		- Remove func_ladder entities from actual ladders. You'll see why if you attempt to play through it.

	- Partial layout revamp. Experiment with mixing and matching existing content to achieve the best result (also look at retail)
		- Remove or reutilize dead ends
		- Remove transition hallway, replace it with ductwork system (see sewer_03_todo.txt)
		- There are very little notable landmarks within this map as it is, its a mess of confusing hallways. Fix this and emphasize the few landmarks there are.

	- Update detail level to become consistent with sewer_03
		- Remove temp/placeholder textures
		- Remake and add treebranch01d.mdl
		- Garbage floating and flowing through the sewer
		- Experiment with lighting - where it needs it. Lighting is crucial in getting the authentic feel of the mod, so the less its changed the better.
		- Environmental storytelling where its appropriate

	- Ambient overhaul for the map
		- Find or create env_soundscape.

	- Bugfixes
		- Find out what's wrong with the water
		- FIx prop_static keyvalues
		- Fix other console spam